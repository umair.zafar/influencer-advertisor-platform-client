import React, { useState, useEffect } from "react";
import {
    Box,
    Modal,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableRow,
    Typography,
    Button,
    Rating,
    FormControl,
} from "@mui/material";
import TextArea from "../../components/common/textarea";
import {
    createPaymentSession,
    submitReview,
    updateOrderStatus,
} from "../../api/order";
import { useSelector } from "react-redux";
import { loadStripe } from "@stripe/stripe-js";
import { CiCreditCard1 } from "react-icons/ci";
import "./style.css"
import { FormSelect } from "react-bootstrap";

const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 800,
    bgcolor: "background.paper",
    boxShadow: 24,
    p: 4,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    borderRadius: "7px",
};

const OrderDetails = ({ open, handleClose, order }) => {
    const user = useSelector((state) => state.user.user);
    const [review, setReview] = useState({ rating: 0, comment: "" });
    const [status, setStatus] = useState(order.status);
    const [showUpdateButton, setShowUpdateButton] = useState(false);

    useEffect(() => {
        setShowUpdateButton(status !== order.status);
    }, [status, order.status]);

    if (!order) {
        return null;
    }

    const statusOptions = {
        requested: ["accepted", "cancelled"],
        accepted: ["pending"],
        pending: [],
        activated: ["completed"],
        completed: [],
        cancelled: [],
        paid: ["activated"],
    };

    const getStatusOptions = () => {
        if (user.role === "advertiser") {
            return statusOptions[order.status].filter(
                (status) => status === "cancelled"
            );
        }
        return statusOptions[order.status];
    };

    const rows = [
        { label: "Order ID", value: order._id },
        { label: "Advertiser Name", value: order.advertiserId?.name },
        { label: "Influencer Name", value: order.influencerId?.name },
        { label: "Price", value: `$ ${order.price} / day` },
        {
            label: "Status",
            value: (
                <FormControl fullWidth>
                    <FormSelect
                        value={status}
                        onChange={(e) => setStatus(e.target.value)}
                    >
                        <option value={order.status}>
                            {order.status.charAt(0).toUpperCase() +
                                order.status.slice(1)}
                        </option>
                        {getStatusOptions().map((statusOption) => (
                            <option key={statusOption} value={statusOption}>
                                {statusOption.charAt(0).toUpperCase() +
                                    statusOption.slice(1)}
                            </option>
                        ))}
                    </FormSelect>
                </FormControl>
            ),
        },
        { label: "Description", value: order.description },
    ];

    const advertiserReview = order.reviews?.find(
        (review) => review.reviewer._id === user._id
    );

    const handleRatingChange = (event, newValue) => {
        setReview({ rating: newValue, comment: review.comment });
    };

    const handleReviewChange = (e) => {
        setReview({ rating: review.rating, comment: e.target.value });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            await submitReview(order._id, {
                rating: review.rating,
                comment: review.comment,
            });

            handleClose();
        } catch (error) {
            console.error("Failed to submit review:", error);
        }
    };

    const handleStatusUpdate = async () => {
        try {
            await updateOrderStatus(order._id, { status });
            window.location.reload();
            handleClose();
        } catch (error) {
            console.error("Failed to update status:", error);
        }
    };

    const handleOrderPayment = async () => {
        const stripe = await loadStripe(
            "pk_test_51PLQgp02Qar5ekPYF3arWOPYU4SJQ7lod2NuKvIOv4N6LlW7RvSnim6Qvdixz8PlteTAgHLWbEOBRrIVSOYjrXsw001dlpWO1r"
        );

        const response = await createPaymentSession(order._id);

        const session = response.data;

        await stripe.redirectToCheckout({
            sessionId: session.id,
        });
    };

    return (
        <Modal
            open={open}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={style} className="order-table">
                <Typography variant="h4" fontWeight={600} marginBottom={2}>
                    Order Details
                </Typography>
                <TableContainer component={Paper} className="paper-component">
                    <Table >
                        <TableBody>
                            {rows.map((row, index) => (
                                <TableRow key={index}>
                                    <TableCell component="th" scope="row">
                                        {row.label}
                                    </TableCell>
                                    <TableCell align="right">
                                        {row.value}
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
                {showUpdateButton && (
                    <Button
                        variant="outlined"
                        disableRipple
                        style={{ borderRadius: "9px" }}
                        onClick={handleStatusUpdate}
                        sx={{ mt: 2 }}
                    >
                        Update Status
                    </Button>
                )}

                {order.status === "pending" && user.role === "advertiser" && (
                    <Button
                        variant="outlined"
                        disableRipple
                        style={{ borderRadius: "9px" }}
                        onClick={handleOrderPayment}
                        sx={{ mt: 2 }}
                    >
                        <CiCreditCard1 size={30} />
                        Pay Now
                    </Button>
                )}
                {order.status === "completed" && order?.reviews[0] && (
                    <Box sx={{ width: "100%", mt: 4 }}>
                        <Typography
                            variant="h5"
                            fontWeight={500}
                            marginBottom={2}
                        >
                            Review
                        </Typography>
                        <Paper sx={{ padding: 2 }} className="paper-component">
                            <Box display="flex" alignItems="center">
                                <img
                                    src={order?.reviews[0].reviewer.avatar}
                                    alt={order?.reviews[0].reviewer.name}
                                    style={{
                                        borderRadius: "50%",
                                        width: 50,
                                        height: 50,
                                        marginRight: 16,
                                    }}
                                />
                                <Box>
                                    <Typography variant="h6">
                                        {order?.reviews[0].reviewer.name}
                                    </Typography>
                                    <Typography
                                        variant="body2"
                                        color="textSecondary"
                                    >
                                        {order?.reviews[0].reviewer.country}
                                    </Typography>
                                    <Rating
                                        value={order?.reviews[0].rating}
                                        readOnly
                                        precision={0.5}
                                    />
                                </Box>
                            </Box>
                            <Typography variant="body1" mt={2}>
                                {order?.reviews[0].comment}
                            </Typography>
                        </Paper>
                    </Box>
                )}
                {user.role === "advertiser" &&
                    order.status === "completed" &&
                    !advertiserReview && (
                        <Box sx={{ width: "100%", mt: 4 }}>
                            <Typography
                                variant="h5"
                                fontWeight={500}
                                marginBottom={2}
                            >
                                Submit Your Review
                            </Typography>
                            <form onSubmit={handleSubmit}>
                                <Box
                                    display="flex"
                                    flexDirection="column"
                                    alignItems="center"
                                >
                                    <Rating
                                        name="user-rating"
                                        value={review.rating}
                                        onChange={handleRatingChange}
                                        size="large"
                                        precision={0.5}
                                    />
                                    <TextArea
                                        label="Review"
                                        value={review.comment}
                                        onChange={handleReviewChange}
                                        rows={4}
                                        fullWidth
                                        required={false}
                                    />
                                    <Button
                                        type="submit"
                                        variant="outlined"
                                        disableRipple
                                        style={{ borderRadius: "9px" }}
                                        color="primary"
                                        sx={{ mt: 2 }}
                                    >
                                        Submit Review
                                    </Button>
                                </Box>
                            </form>
                        </Box>
                    )}
            </Box>
        </Modal>
    );
};

export default OrderDetails;
