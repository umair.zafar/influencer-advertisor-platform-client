import React from "react";
import "./style.css";
import { Rating } from "@mui/material";

const Reviews = ({review}) => {
    return (
        <>
            <div className="review-item rounded p-3">
                <div className="flex gap-3 flex-center mt-2">
                    <img
                        src={review?.reviewer?.avatar || "images/3135715.png"}
                        alt="avatar"
                        width={60}
                        height={60}
                    />
                    <div>
                        <div>
                            <span>{review.reviewer.name}</span>
                        </div>
                        <div>
                            <span>{review.reviewer.country}</span>
                        </div>
                    </div>
                </div>
                <div className="mt-2">
                    <Rating
                        value={review.rating}
                        readOnly
                        precision={0.5}
                    />
                </div>
                <div className="mt-2">
                    {review.comment}
                </div>
            </div>
            <hr />
        </>
    );
};

export default Reviews;
