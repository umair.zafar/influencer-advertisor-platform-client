import React from "react";
import "./style.css";
import { Rating } from "@mui/material";

const UserDetails = ({ gig }) => {
    return (
        <div className="user-detailed-section p-4 common-shadow">
            <div className="flex gap-2 flex-center">
                <img src={gig.userId.avatar} alt="" width={60} height={60} />
                <div>
                    <div>
                        <span>{gig.userId.name}</span>
                    </div>
                    <div>
                        <Rating value={gig.averageRating} readOnly precision={0.5} />
                    </div>
                </div>
            </div>
            <div>
                <div className="col mt-3">Followers</div>
                <div className="flex space-between flex-center">
                    <div className="progress col">
                        <div
                            className="progress-bar bg-success"
                            role="progressbar"
                            style={{ width: "95%" }}
                            aria-valuemin="0"
                            aria-valuemax="100"
                        ></div>
                    </div>
                    <span>{gig.followers}</span>
                </div>
                <div className="col mt-3">Total Likes</div>
                <div className="flex space-between flex-center">
                    <div className="progress col">
                        <div
                            className="progress-bar bg-info"
                            role="progressbar"
                            style={{ width: "65%" }}
                            aria-valuemin="0"
                            aria-valuemax="100"
                        ></div>
                    </div>
                    <span>{gig.likes}</span>
                </div>
                <div className="col mt-3">Total Comments</div>
                <div className="flex space-between flex-center">
                    <div className="progress col">
                        <div
                            className="progress-bar bg-warning"
                            role="progressbar"
                            style={{ width: "35%" }}
                            aria-valuemin="0"
                            aria-valuemax="100"
                        ></div>
                    </div>
                    <span>{gig.comments}</span>
                </div>
            </div>
        </div>
    );
};

export default UserDetails;
