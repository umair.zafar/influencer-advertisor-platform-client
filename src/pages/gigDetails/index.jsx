import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import "./style.css";
import UserDetails from "./userDetails";
import Description from "./description";
import Reviews from "./reviews";
import { Rating, Modal, Box, Button } from "@mui/material";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { BsChatFill } from "react-icons/bs";
import { IoIosCreate } from "react-icons/io";
import { fetchGigDetails } from "../../api/gigs";
import { useSelector } from "react-redux";
import { createOrder } from "../../api/order";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import InputField from "../../components/common/inputField";
import TextArea from "../../components/common/textarea";
import { FormSelect } from "react-bootstrap";

const GigDetails = () => {
    const { id } = useParams();
    const [gigDetails, setGigDetails] = useState(null);
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);
    const [isModalOpen, setIsModalOpen] = useState(false);
    const user = useSelector((state) => state.user.user);

    useEffect(() => {
        const getGigDetails = async () => {
            try {
                const data = await fetchGigDetails(id);
                setGigDetails(data.gig);
            } catch (error) {
                setError("Failed to fetch gig details");
            } finally {
                setLoading(false);
            }
        };

        getGigDetails();
    }, [id]);

    const handleOrderNow = () => {
        setIsModalOpen(true);
    };

    const handleCloseModal = () => {
        setIsModalOpen(false);
    };

    const validationSchema = Yup.object({
        price: Yup.number().required("Price is required"),
        description: Yup.string().required("Description is required"),
        type: Yup.string().required("Order type is required"),
        startingDate: Yup.date().required("Starting date is required"),
        endingDate: Yup.date()
            .required("Ending date is required")
            .min(
                Yup.ref("startingDate"),
                "Ending date can't be before starting date"
            ),
    });

    if (loading) {
        return <div>Loading...</div>;
    }

    if (error) {
        return <div>{error}</div>;
    }

    return (
        <div className="container">
            <div className="w-full flex gap-5 mt-10">
                {user?.role === "advertiser" && (
                    <>
                        <Link
                            to={`/chat?receiverId=${gigDetails.userId._id}`}
                            className="chat-btn btn"
                        >
                            <span className="m-2">Chat Now</span>
                            <BsChatFill className="m-2" />
                        </Link>
                        <Link
                            className="order-btn btn"
                            onClick={handleOrderNow}
                        >
                            <span className="m-2">Order Now</span>
                            <IoIosCreate />
                        </Link>
                    </>
                )}

                <div className="left-section mt-4">
                    <h1 className="title text-component-item">
                        {gigDetails?.title}
                    </h1>
                    <div className="flex gap-2 flex-center mt-4 text-component-item">
                        <img
                            src={gigDetails?.userId.avatar}
                            alt="avatar"
                            className="avatar"
                        />
                        <div>
                            <div className="user-name">
                                {gigDetails?.userId.name}
                            </div>
                            <div>
                                <Rating
                                    name="read-only-rating"
                                    value={gigDetails?.averageRating}
                                    readOnly
                                    precision={0.5}
                                />
                            </div>
                        </div>
                    </div>
                    <img
                        src={gigDetails?.banner}
                        alt="banner"
                        className="banner-img mt-4 component-item"
                    />

                    <div className="component-item">
                        <Description description={gigDetails?.description} />
                    </div>

                    <div className="mt-5">
                        <h3>Reviews</h3>
                    </div>
                    {gigDetails?.reviews.map((review, i) => (
                        <Reviews key={i} review={review} />
                    ))}
                </div>

                <div className="right-section">
                    <UserDetails gig={gigDetails} />
                </div>
            </div>

            {user?.role === "advertiser" && (
                <Modal
                    open={isModalOpen}
                    onClose={handleCloseModal}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box className="modal-box">
                        <h2 id="modal-modal-title">Request Order</h2>
                        <hr />
                        <Formik
                            initialValues={{
                                price: gigDetails?.price,
                                description: "",
                                type: "story",
                                startingDate: new Date(
                                    new Date().setDate(new Date().getDate() + 1)
                                ),
                                endingDate: new Date(
                                    new Date().setDate(new Date().getDate() + 2)
                                ),
                            }}
                            validationSchema={validationSchema}
                            onSubmit={async (values, { setSubmitting }) => {
                                const orderData = {
                                    advertiserId: user._id,
                                    influencerId: gigDetails?.userId._id,
                                    price: values.price,
                                    description: values.description,
                                    type: values.type,
                                    startingDate:
                                        values.startingDate.toISOString(),
                                    endingDate: values.endingDate.toISOString(),
                                    gigId: id,
                                };
                                await createOrder(orderData);
                                setIsModalOpen(false);
                                setSubmitting(false);
                            }}
                        >
                            {({ setFieldValue, values, isSubmitting }) => (
                                <Form>
                                    <Field
                                        as={InputField}
                                        label="$ Price/day"
                                        fullWidth
                                        readOnly={true}
                                        value={gigDetails?.price}
                                        margin="normal"
                                    />
                                    <ErrorMessage
                                        name="price"
                                        component="div"
                                        className="error"
                                    />

                                    <Field
                                        as={TextArea}
                                        label="Description"
                                        name="description"
                                        fullWidth
                                        margin="normal"
                                    />
                                    <ErrorMessage
                                        name="description"
                                        component="div"
                                        className="error"
                                    />

                                    <label htmlFor="type">Order Type</label>
                                    <Field
                                        as={FormSelect}
                                        name="type"
                                        fullWidth
                                        margin="normal"
                                        value={values.type}
                                        onChange={(e) =>
                                            setFieldValue(
                                                "type",
                                                e.target.value
                                            )
                                        }
                                    >
                                        <option value="story">Story</option>
                                        <option value="reel">Reel</option>
                                        <option value="post">Post</option>
                                        <option value="highlights">
                                            Highlights
                                        </option>
                                    </Field>
                                    <ErrorMessage
                                        name="type"
                                        component="div"
                                        className="error"
                                    />

                                    <label
                                        htmlFor="startingDate"
                                        className="mt-3"
                                    >
                                        Starting Date
                                    </label>
                                    <br />
                                    <DatePicker
                                        name="startingDate"
                                        selected={values.startingDate}
                                        onChange={(date) =>
                                            setFieldValue("startingDate", date)
                                        }
                                        dateFormat="yyyy/MM/dd"
                                        className="startingDate"
                                        minDate={
                                            new Date(
                                                new Date().setDate(
                                                    new Date().getDate() + 1
                                                )
                                            )
                                        }
                                    />
                                    <br />
                                    <ErrorMessage
                                        name="startingDate"
                                        component="div"
                                        className="error"
                                    />

                                    <label
                                        htmlFor="endingDate"
                                        className="mt-3"
                                    >
                                        Ending Date
                                    </label>
                                    <br />
                                    <DatePicker
                                        name="endingDate"
                                        selected={values.endingDate}
                                        onChange={(date) =>
                                            setFieldValue("endingDate", date)
                                        }
                                        dateFormat="yyyy/MM/dd"
                                        className="endingDate"
                                        minDate={
                                            new Date(
                                                new Date().setDate(
                                                    new Date().getDate() + 1
                                                )
                                            )
                                        }
                                    />
                                    <ErrorMessage
                                        name="endingDate"
                                        component="div"
                                        className="error"
                                    />

                                    <Button
                                        type="submit"
                                        variant="outlined"
                                        disableRipple
                                        style={{ borderRadius: "9px" }}
                                        className="mt-4"
                                        fullWidth
                                        disabled={isSubmitting}
                                    >
                                        Request Order
                                    </Button>
                                </Form>
                            )}
                        </Formik>
                    </Box>
                </Modal>
            )}
        </div>
    );
};

export default GigDetails;
