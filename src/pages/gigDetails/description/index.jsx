import React from "react";
import "./style.css";

const Description = ({description}) => {
    return (
        <div className="description mt-4 mb-3 p-4">
            <h3>Description</h3>
            <p>
                {description}
            </p>
        </div>
    );
};

export default Description;
