import React, { useState } from "react";
import DataTable from "react-data-table-component";
import OrderDetails from "../../orderDetails";
import { FaArrowRight } from "react-icons/fa";
import { Button } from "@mui/material";

const columns = (handleOpen) => [
    {
        name: "Influencer Name",
        selector: (row) => row.influencerId.name,
    },
    {
        name: "Advertiser Name",
        selector: (row) => row.advertiserId.name,
    },
    {
        name: "Price",
        selector: (row) => `$ ${row.price} / day`,
    },
    {
        name: "Status",
        selector: (row) => row.status,
    },
    {
        name: "Details",
        cell: (row) => (
            <Button className="btn" onClick={() => handleOpen(row)}>
                <FaArrowRight />
            </Button>
        ),
    },
];

const CancelledOrders = ({ orders }) => {
    const [open, setOpen] = useState(false);
    const [selectedOrder, setSelectedOrder] = useState(null);

    const handleOpen = (order) => {
        setSelectedOrder(order);
        setOpen(true);
    };

    const handleClose = () => {
        setSelectedOrder(null);
        setOpen(false);
    };

    return (
        <div>
            <div className="accordion" id="pendingOrders">
                <div className="accordion-item">
                    <h2 className="accordion-header" id="pendingHeading">
                        <button
                            className="accordion-button"
                            type="button"
                            data-bs-toggle="collapse"
                            data-bs-target="#cancelledOrdersCollapse"
                            aria-expanded="true"
                            aria-controls="cancelledOrdersCollapse"
                        >
                            Cancelled Orders ({orders.length})
                        </button>
                    </h2>
                    <div
                        id="cancelledOrdersCollapse"
                        className="accordion-collapse collapse show"
                        aria-labelledby="pendingHeading"
                        data-bs-parent="#pendingOrders"
                    >
                        <div className="accordion-body">
                            <DataTable
                                columns={columns(handleOpen)}
                                data={orders}
                                pagination
                                paginationPerPage={5}
                                paginationRowsPerPageOptions={[5, 10, 20]}
                                paginationComponentOptions={{
                                    rowsPerPageText: 'Rows per page:',
                                    rangeSeparatorText: 'of',
                                    noRowsPerPage: false,
                                    selectAllRowsItem: true,
                                    selectAllRowsItemText: 'All',
                                }}
                            />
                        </div>
                    </div>
                </div>
            </div>
            {open && selectedOrder && (
                <OrderDetails
                    order={selectedOrder}
                    open={open}
                    handleClose={handleClose}
                />
            )}
        </div>
    );
};

export default CancelledOrders;
