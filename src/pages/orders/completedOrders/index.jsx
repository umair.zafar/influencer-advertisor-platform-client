import React, { useState } from "react";
import DataTable from "react-data-table-component";
import { FaArrowRight } from "react-icons/fa";
import OrderDetails from "../../orderDetails";
import { Button, Rating } from "@mui/material";

const columns = (handleOpen) => [
    {
        name: "Influencer Name",
        selector: (row) => row.influencerId.name,
    },
    {
        name: "Advertiser Name",
        selector: (row) => row.advertiserId.name,
    },
    {
        name: "Price",
        selector: (row) => `$ ${row.price} / day`,
    },
    {
        name: "Rating",
        selector: (row) => row.reviews[0]?.rating,
        cell: (row) => (
            <Rating value={row.reviews[0]?.rating} readOnly precision={0.5} />
        ),
    },
    {
        name: "Details",
        cell: (row) => (
            <Button className="btn" onClick={() => handleOpen(row)}>
                <FaArrowRight />
            </Button>
        ),
    },
];

const CompletedOrders = ({ orders, userId }) => {
    const [open, setOpen] = useState(false);
    const [selectedOrder, setSelectedOrder] = useState(null);

    const handleOpen = (order) => {
        setSelectedOrder(order);
        setOpen(true);
    };

    const handleClose = () => {
        setSelectedOrder(null);
        setOpen(false);
    };

    return (
        <div>
            <div className="accordion" id="completedOrders">
                <div className="accordion-item">
                    <h2 className="accordion-header" id="completedHeading">
                        <button
                            className="accordion-button"
                            type="button"
                            data-bs-toggle="collapse"
                            data-bs-target="#completedOrdersCollapse"
                            aria-expanded="true"
                            aria-controls="completedOrdersCollapse"
                        >
                            Completed Orders ({orders.length})
                        </button>
                    </h2>
                    <div
                        id="completedOrdersCollapse"
                        className="accordion-collapse collapse show"
                        aria-labelledby="completedHeading"
                        data-bs-parent="#completedOrders"
                    >
                        <div className="accordion-body">
                            <DataTable
                                columns={columns(handleOpen)}
                                data={orders}
                                pagination
                                paginationPerPage={5}
                                paginationRowsPerPageOptions={[5, 10, 20]}
                                paginationComponentOptions={{
                                    rowsPerPageText: "Rows per page:",
                                    rangeSeparatorText: "of",
                                    noRowsPerPage: false,
                                    selectAllRowsItem: true,
                                    selectAllRowsItemText: "All",
                                }}
                            />
                        </div>
                    </div>
                </div>
            </div>
            {open && selectedOrder && (
                <OrderDetails
                    order={selectedOrder}
                    open={open}
                    handleClose={handleClose}
                />
            )}
        </div>
    );
};

export default CompletedOrders;
