import React, { useEffect, useState } from "react";
import PendingOrders from "./pendingOrders";
import CompletedOrders from "./completedOrders";
import { fetchOrders } from "../../api/order";
import CancelledOrders from "./cancelledOrders";

const Orders = () => {
    const [pendingOrders, setPendingOrders] = useState([]);
    const [completedOrders, setCompletedOrders] = useState([]);
    const [cancelledOrders, setCancelledOrders] = useState([])
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        const loadOrders = async () => {
            try {
                const orders = await fetchOrders();
                const pending = orders.filter(
                    (order) => (order.status !== "completed" && order.status !== "cancelled")
                );

                const cancelled = orders.filter(
                    (order) => order.status === "cancelled"
                );
                const completed = orders.filter(
                    (order) => order.status === "completed"
                );
                setPendingOrders(pending);
                setCancelledOrders(cancelled);
                setCompletedOrders(completed);
            } catch (error) {
                console.error("Error loading orders", error);
            } finally {
                setLoading(false);
            }
        };

        loadOrders();
    }, []);

    if (loading) {
        return <div>Loading...</div>;
    }

    return (
        <div className="container mt-10">
            <h1>Orders</h1>
            <br />
            <PendingOrders orders={pendingOrders} />
            <br />
            <CompletedOrders orders={completedOrders} />
            <br />
            <CancelledOrders orders={cancelledOrders} />
        </div>
    );
};

export default Orders;
