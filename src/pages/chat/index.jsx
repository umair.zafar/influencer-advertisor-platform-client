/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState, useRef } from "react";
import "./style.css";
import InputField from "../../components/common/inputField";
import { IoIosArrowBack, IoMdSend } from "react-icons/io";
import { listenForMessages, sendMessage } from "../../api/messages";
import { useSelector } from "react-redux";
import { useLocation } from "react-router-dom";
import { addChat, getAllChats } from "../../api/user";
import { Button } from "@mui/material";
import { FaSearch } from "react-icons/fa";

const useQuery = () => {
    return new URLSearchParams(useLocation().search);
};

const Chat = () => {
    const [activeUserId, setActiveUserId] = useState("");
    const [activeUser, setActiveUser] = useState("");
    const [messages, setMessages] = useState([]);
    const [message, setMessage] = useState("");
    const [chats, setChats] = useState([]);
    const [activeChat, setActiveChat] = useState("");
    const [searchContact, setSearchContact] = useState("");

    const user = useSelector((state) => state.user.user);
    const query = useQuery();
    const receiverId = query.get("receiverId") || "";

    const messageDisplayRef = useRef(null);

    const getChats = async () => {
        try {
            const response = await getAllChats(searchContact);
            setChats(response.chats);

            if (response.chats && response.chats.length > 0) {
                response.chats.forEach((chat) => {
                    if (chat.chatterId._id === activeUserId) {
                        setActiveUser(chat.chatterId);
                        setActiveChat(chat.chatId);
                    }
                });
            }
        } catch (error) {}
    };

    useEffect(() => {
        if (receiverId) {
            setActiveUserId(receiverId);
        }
        getChats();
    }, [receiverId]);

    useEffect(() => {
        getChats();
        let unsubscribe;
        if (activeUserId) {
            unsubscribe = listenForMessages(activeChat, setMessages);
        }
        return () => {
            if (unsubscribe) unsubscribe();
        };
    }, [activeUserId, activeChat]);

    useEffect(() => {
        if (messageDisplayRef.current) {
            messageDisplayRef.current.scrollTop =
                messageDisplayRef.current.scrollHeight;
        }
    }, [messages]);

    const handleSendMessage = async () => {
        if (message.trim()) {
            await sendMessage(activeChat, user._id, activeUserId, message);
            setMessage("");
        }
    };

    const createChat = async () => {
        await addChat(activeUserId);
        await getChats();
    };

    const isChatExists = chats.some(
        (chat) => chat.chatterId._id === activeUserId
    );

    return (
        <div className="container mt-10">
            <div className="chat-box">
                {/* Left Side | Participants */}
                <div className="contact-bar">
                    <div className="chat-search-box flex flex-center gap-2">
                        <InputField
                            placeholder="Search"
                            value={searchContact}
                            onChange={(e) => setSearchContact(e.target.value)}
                        />
                        <Button
                            variant="outlined"
                            color="grey"
                            disableRipple
                            style={{ height: "40px", borderRadius: "10px" }}
                            onClick={getChats}
                        >
                            <FaSearch size={20} />
                        </Button>
                    </div>
                    <div className="contact-list">
                        {chats.length > 0 &&
                            chats.map((chat) => (
                                <div
                                    className={`single-contact ${
                                        chat.chatId === activeChat
                                            ? "active-chat"
                                            : undefined
                                    }`}
                                    key={chat._id}
                                    onClick={() => {
                                        setActiveUserId(chat.chatterId._id);
                                        setActiveChat(chat.chatId);
                                    }}
                                >
                                    <img
                                        src={
                                            chat.chatterId.avatar ??
                                            "images/3135715.png"
                                        }
                                        alt="ava"
                                        width={30}
                                        height={30}
                                    />
                                    <div className="right-section-name">
                                        <div className="chat-name">
                                            {chat.chatterId.name}
                                        </div>
                                        <div className="chat-id">
                                            {chat.updatedAt}
                                        </div>
                                    </div>
                                </div>
                            ))}
                    </div>
                </div>

                {/* Right Side | Chat */}
                {activeUserId ? (
                    isChatExists ? (
                        <div className="chat-section">
                            <div className="exit-chat-btn">
                                <Button
                                    color="grey"
                                    onClick={() => {
                                        setActiveChat(null);
                                        setActiveUserId(null);
                                    }}
                                >
                                    <IoIosArrowBack size={30} /> <b>Close</b>
                                </Button>
                                <img
                                    src={
                                        activeUser.avatar ??
                                        "images/3135715.png"
                                    }
                                    alt="ava"
                                    width={30}
                                    height={30}
                                    className="m-2"
                                />
                                <span>{activeUser.name}</span>
                            </div>
                            <div
                                className="message-display-section"
                                ref={messageDisplayRef}
                            >
                                {messages.map((message, index) => (
                                    <div
                                        className={`message ${
                                            message.senderId === activeUserId
                                                ? "recieved-message"
                                                : "sent-message"
                                        }`}
                                        key={index}
                                    >
                                        <div>
                                            <p>{message.text}</p>
                                            <span>
                                                {new Date(
                                                    message.timestamp
                                                ).toLocaleString()}
                                            </span>
                                        </div>
                                    </div>
                                ))}
                            </div>

                            <div className="message-type-section">
                                <InputField
                                    placeholder="Type a message"
                                    value={message}
                                    onChange={(e) => setMessage(e.target.value)}
                                />
                                <Button
                                    disableElevation
                                    disableRipple
                                    onClick={handleSendMessage}
                                >
                                    <IoMdSend size={30} />
                                </Button>
                            </div>
                        </div>
                    ) : (
                        <div className="chat-select-message">
                            <Button
                                variant="outlined"
                                style={{ borderRadius: "9px" }}
                                disableElevation
                                disableRipple
                                onClick={createChat}
                            >
                                Create Chat
                            </Button>
                        </div>
                    )
                ) : (
                    <div className="chat-select-message">
                        Choose a contact to chat
                    </div>
                )}
            </div>
        </div>
    );
};

export default Chat;
