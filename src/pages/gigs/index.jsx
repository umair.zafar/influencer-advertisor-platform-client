/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import "./style.css";
import { fetchAllGigs } from "../../api/gigs";
import GigCard from "../../components/common/gigCard";
import InputField from "../../components/common/inputField";
import { FaSearch } from "react-icons/fa";
import { Button } from "@mui/material";

const useQuery = () => {
    return new URLSearchParams(useLocation().search);
};

const Gigs = () => {
    const query = useQuery();
    const initialSearch = query.get("search") || "";

    const [gigs, setGigs] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [totalPages, setTotalPages] = useState(1);
    const [search, setSearch] = useState(initialSearch);
    const [minBudget, setMinBudget] = useState("");
    const [maxBudget, setMaxBudget] = useState("");

    const getGigs = async (page, searchTerm) => {
        try {
            const { gigs, totalPages } = await fetchAllGigs({
                page,
                search: searchTerm,
                minBudget,
                maxBudget,
            });
            setGigs(gigs);
            setTotalPages(totalPages);
        } catch (error) {
            console.error("Error fetching gigs:", error);
        }
    };
    
    useEffect(() => {
        getGigs(currentPage, search);
    }, [currentPage]);

    const handlePreviousPage = () => {
        if (currentPage > 1) setCurrentPage(currentPage - 1);
    };

    const handleNextPage = () => {
        if (currentPage < totalPages) setCurrentPage(currentPage + 1);
    };

    const handleSearch = () => {
        getGigs(1, search);
    };

    const handleSearchChange = (e) => setSearch(e.target.value);
    const handleMinBudgetChange = (e) => setMinBudget(e.target.value);
    const handleMaxBudgetChange = (e) => setMaxBudget(e.target.value);

    return (
        <div className="container mt-10">
            {/* Filters */}
            <div className="filters-box common-shadow">
                <h4>Filters</h4>
                <div className="filters">
                    <InputField
                        label="Search"
                        value={search}
                        name="search"
                        placeholder="Search..."
                        onChange={handleSearchChange}
                        className="search-input"
                    />
                    <InputField
                        label="Min Budget ($)"
                        value={minBudget}
                        name="minBudget"
                        type="number"
                        placeholder="Min Budget"
                        onChange={handleMinBudgetChange}
                        prefix="$"
                    />
                    <InputField
                        label="Max Budget ($)"
                        value={maxBudget}
                        name="maxBudget"
                        type="number"
                        placeholder="Max Budget"
                        onChange={handleMaxBudgetChange}
                        prefix="$"
                    />
                    <Button
                        variant="outlined"
                        disableRipple
                        style={{ borderRadius: "9px" }}
                        onClick={handleSearch}
                        className="search-btn"
                    >
                        <FaSearch />
                    </Button>
                </div>
            </div>


            <div className="gig-list">
                {gigs && gigs.map((gig) => <GigCard key={gig._id} gig={gig} />)}
            </div>
            <div className="pagination">
                <button
                    onClick={handlePreviousPage}
                    disabled={currentPage === 1}
                >
                    Previous
                </button>
                <span>
                    Page {currentPage} of {totalPages}
                </span>
                <button
                    onClick={handleNextPage}
                    disabled={currentPage === totalPages}
                >
                    Next
                </button>
            </div>
        </div>
    );
};

export default Gigs;
