import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { FaArrowRight, FaSearch } from "react-icons/fa";
import FAQ from "../faq";
import "./landing.css";

const Landing = () => {
    const [searchTerm, setSearchTerm] = useState("");
    const navigate = useNavigate();

    const handleSearchChange = (e) => {
        setSearchTerm(e.target.value);
    };

    const handleSearch = () => {
        if (searchTerm.trim()) {
            navigate(`/gigs?search=${searchTerm}`);
        }
    };

    return (
        <div>
            {/* Hero Section */}
            <section id="hero" className="hero-section">
                <div className="hero-content">
                    <div className="left-side">
                        <h1><strong>InfluencerX</strong></h1>
                        <p>PROMOTION MADE EASY!</p>
                        <p>Contact An Influencer.</p>
                        <p>
                            In The Ever-Evolving Landscape Of Skills And
                            Knowledge, The Choice Between Hiring An Expert Or
                            Becoming One Yourself Is A Pivotal Decision.
                        </p>
                        <div className="search-bar">
                            <input
                                type="text"
                                placeholder='Search To "Find Freelancers, Jobs, Or Services"'
                                value={searchTerm}
                                onChange={handleSearchChange}
                            />
                            <button onClick={handleSearch}>
                                <FaSearch />
                            </button>
                        </div>
                    </div>
                    <div className="right-side">
                        <img
                            src="images/hero-image.png"
                            alt="Hero"
                            className="hero-image"
                        />
                    </div>
                </div>
            </section>

            {/* Services Section - 4 images */}
            <section id="services" className="services-section simple-container">
                <h2>Here are Something You'd Need</h2>
                <div className="services-content">
                    <div className="service-item">
                        <img src="images/reel.png" alt="Reels" />
                    </div>
                    <div className="service-item">
                        <img src="images/story.png" alt="Stories" />
                    </div>
                    <div className="service-item">
                        <img src="images/igtv.png" alt="IGTV" />
                    </div>
                    <div className="service-item">
                        <img src="images/post.png" alt="Posts" />
                    </div>
                </div>
            </section>

            {/* Feature Section */}
            <section id="features" className="feature-section">
                <div className="feature-section-actual simple-container">
                    <div className="feature-content">
                        <h2>Discover Our Outstanding Features</h2>
                        <div className="feature-item">
                            <h3>Feature 1</h3>
                            <p>Detail about feature 1.</p>
                        </div>
                        <div className="feature-item">
                            <h3>Feature 2</h3>
                            <p>Detail about feature 2.</p>
                        </div>
                        <div className="feature-item">
                            <h3>Feature 3</h3>
                            <p>Detail about feature 3.</p>
                        </div>
                    </div>
                    <div className="feature-image">
                        <img src="images/feature.png" alt="Features" />
                    </div>
                </div>
            </section>

            {/* FAQ Section */}
            <section id="faq" className="faq-section">
                <FAQ />
            </section>

            {/* Ready to get Started */}
            <section id="cta" className="cta-section">
                <h2>Ready to Get Started?</h2>
                <p>Join us today and start enhancing your brand's reach.</p>
                <button className="cta-button">Grow Together  <FaArrowRight /> </button>
            </section>

            {/* Trending Sellers */}
            <section id="trending-sellers" className="trending-sellers-section">
                <h2>{/* Trending Sellers */}</h2>
                {/* <TrendingSellers /> */}
            </section>
        </div>
    );
};

export default Landing;
