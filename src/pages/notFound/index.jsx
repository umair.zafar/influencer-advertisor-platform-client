import React from "react";
import "./style.css"

const NotFound = () => {
    return (
        <div className="not-found-body">
            <div className="center-div">
                <h6 className="label">
                    404 | NOT FOUND
                </h6>
            </div>
        </div>
    );
};

export default NotFound;
