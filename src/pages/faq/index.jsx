import React from "react";
import "./style.css"

const faqs = [
    {
        id: 1,
        question: "What is Lorem Ipsum?",
        answer: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    },
    {
        id: 2,
        question: "Why do we use it?",
        answer: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.",
    },
    {
        id: 3,
        question: "Where does it come from?",
        answer: "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC.",
    },
    {
        id: 4,
        question: "Where can I get some?",
        answer: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.",
    },
    {
        id: 5,
        question: "What is the origin of Lorem Ipsum?",
        answer: "The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested.",
    },
    {
        id: 6,
        question: "Is Lorem Ipsum random text?",
        answer: "No, Lorem Ipsum is derived from a piece of classical Latin literature from 45 BC, making it over 2000 years old.",
    },
    {
        id: 7,
        question: "Why do we use Lorem Ipsum?",
        answer: "It is used as placeholder text because it has a more-or-less normal distribution of letters, making it look like readable English.",
    },
    {
        id: 8,
        question: "Can I use Lorem Ipsum for my project?",
        answer: "Yes, Lorem Ipsum is free to use for any purpose, including personal and commercial projects.",
    },
    {
        id: 9,
        question: "Are there different versions of Lorem Ipsum?",
        answer: "Yes, there are many variations, but most have been altered in some way, often by injected humor or random words.",
    },
    {
        id: 10,
        question: "Is Lorem Ipsum the same as English?",
        answer: "No, although it looks like readable English, it is actually a mixture of Latin words with a scrambled structure.",
    },
];

const FAQ = () => {
    return (
        <div className="container mt-10">
            <h1>Frequently Asked Questions</h1>
            <div className="accordion accordion-flush mt-3" id="faq">
                {faqs.map((faq) => (
                    <div className="accordion-item" key={faq.id}>
                        <h2
                            className="accordion-header"
                            id={`heading${faq.id}`}
                        >
                            <button
                                className="accordion-button"
                                type="button"
                                data-bs-toggle="collapse"
                                data-bs-target={`#collapse${faq.id}`}
                                aria-expanded="false"
                                aria-controls={`collapse${faq.id}`}
                            >
                                {faq.question}
                            </button>
                        </h2>
                        <div
                            id={`collapse${faq.id}`}
                            className="accordion-collapse collapse"
                            aria-labelledby={`heading${faq.id}`}
                            data-bs-parent="#faq"
                        >
                            <div className="accordion-body">{faq.answer}</div>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
};

export default FAQ;
