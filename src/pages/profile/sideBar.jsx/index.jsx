import React from "react";
import { AiOutlineLogout } from "react-icons/ai";
import { RiLockPasswordLine } from "react-icons/ri";
import { FaBusinessTime } from "react-icons/fa";
import "./style.css";
import { useDispatch, useSelector } from "react-redux";
import { logout } from "../../../features/user/userSlice";
import { logoutUser } from "../../../api/user";

const SideBar = ({ active, setActive }) => {
    const user = useSelector((state) => state.user.user);

    const dispatch = useDispatch();

    const logOutHandler = async () => {
        try {
            await logoutUser(dispatch);
            dispatch(logout());
        } catch (error) {
            console.error("Logout failed", error);
        }
    };

    return (
        <div className="w-full">
            <div
                className={`flex menu-item ${
                    active === 1 ? "active-menu-item" : undefined
                }`}
                onClick={() => setActive(1)}
            >
                <img
                    src={user?.avatar || "images/3135715.png"}
                    alt=""
                    height={30}
                    width={30}
                    className="menu-image"
                />
                <span className="menu-label">My Account</span>
            </div>
            <div
                className={`flex gap-2 menu-item ${
                    active === 2 ? "active-menu-item" : undefined
                }`}
                onClick={() => setActive(2)}
            >
                <RiLockPasswordLine size={25} className="menu-image" />
                <span className="menu-label">Change Password</span>
            </div>
            {user?.role === "influencer" && (
                <div
                    className={`flex gap-2 menu-item ${
                        active === 3 ? "active-menu-item" : undefined
                    }`}
                    onClick={() => setActive(3)}
                >
                    <FaBusinessTime size={25} className="menu-image" />
                    <span className="menu-label">Gig Details</span>
                </div>
            )}
            <div className={`flex gap-2 menu-item`} onClick={logOutHandler}>
                <AiOutlineLogout
                    size={25}
                    className="menu-image"
                    fill="crimson"
                />
                <span className="menu-label">Log Out</span>
            </div>
        </div>
    );
};

export default SideBar;
