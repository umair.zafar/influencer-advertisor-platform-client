import React, { useState } from "react";
import { MdEdit } from "react-icons/md";
import { useDispatch, useSelector } from "react-redux";
import InputField from "../../../components/common/inputField";
import {
    updateUser,
    uploadAvatarThunk,
} from "../../../features/user/userSlice";
import "./style.css";
import { Button } from "@mui/material";
import { LoadingButton } from "@mui/lab";

const ProfileInfo = () => {
    const dispatch = useDispatch();
    const user = useSelector((state) => state.user.user);

    const [name, setName] = useState(user?.name || "");
    const [email, setEmail] = useState(user?.email || "");
    const [facebookId, setFacebookId] = useState(user?.facebookId || "");
    const [city, setCity] = useState(user?.city || "");
    const [country, setCountry] = useState(user?.country || "");
    const [avatar, setAvatar] = useState(user?.avatar || "");
    const [loading, setLoading] = useState(false);

    const imageHandler = async (e) => {
        const file = e.target.files[0];
        if (file) {
            setLoading(true);
            const formData = new FormData();
            formData.append("avatar", file);

            try {
                const response = await dispatch(uploadAvatarThunk(formData));
                setAvatar(response.payload.avatar);
            } catch (error) {
                console.error("Failed to upload avatar", error);
            }
            setLoading(false);
        }
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const updatedUser = await dispatch(
                updateUser({ name, city, country })
            ).unwrap();
            setCity(updatedUser.city);
            setCountry(updatedUser.country);
        } catch (error) {
            console.error("Failed to update profile", error);
        }
    };

    return (
        <>
            <div className="w-full flex justify-center">
                <div className="relative">
                    <LoadingButton loading={loading}>
                        <label htmlFor="avatar">
                            {!loading && (
                                <img
                                    src={avatar}
                                    alt=""
                                    width={120}
                                    height={120}
                                    className="image-update"
                                />
                            )}
                            <div className="edit-icon">
                                <MdEdit size={20} />
                            </div>
                        </label>
                        <input
                            type="file"
                            id="avatar"
                            hidden
                            onChange={imageHandler}
                            accept="image/png,image/jpg,image/jpeg,image/webp"
                        />
                    </LoadingButton>
                </div>
            </div>
            <br />
            <br />
            <div>
                <form onSubmit={handleSubmit}>
                    <div className="input-field-area ">
                        <InputField
                            label="Full Name"
                            value={name}
                            onChange={(e) => setName(e.target.value)}
                        />
                        <InputField
                            label="Email Address"
                            value={email}
                            readonly={true}
                            onChange={(e) => setEmail(e.target.value)}
                        />
                        <InputField
                            label="Facebook ID"
                            readonly={true}
                            value={facebookId}
                            onChange={(e) => setFacebookId(e.target.value)}
                        />
                        <InputField
                            label="City"
                            value={city}
                            onChange={(e) => setCity(e.target.value)}
                        />
                        <InputField
                            label="Country"
                            value={country}
                            onChange={(e) => setCountry(e.target.value)}
                        />
                        <div>
                            <input
                                type="submit"
                                value="Update"
                                required
                                className="submit-button"
                            />
                        </div>
                    </div>
                </form>
            </div>
        </>
    );
};

export default ProfileInfo;
