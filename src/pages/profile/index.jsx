import React, { useState } from "react";
import SideBar from "./sideBar.jsx/index.jsx";
import ProfileInfo from "./profileInfo/index.jsx";
import ChangePassword from "./changePassword/index.jsx";
import "./style.css";
import EditGigDetails from "./editGigDetails/index.jsx";

const Profile = () => {
    const [active, setActive] = useState(1);

    return (
        <div className="flex">
            {/* Sidebar | Left Side*/}
            <div className="sidebar-profile-menu">
                <SideBar active={active} setActive={setActive} />
            </div>

            {/* Content Area | Right Side */}
            <div className="w-full content-area-profile">
                {active === 1 && <ProfileInfo />}
                {active === 2 && <ChangePassword />}
                {active === 3 && <EditGigDetails />}
            </div>
        </div>
    );
};

export default Profile;
