import React, { useState, useEffect } from "react";
import "./style.css";
import { MdEdit } from "react-icons/md";
import InputField from "../../../components/common/inputField";
import TextArea from "../../../components/common/textarea";
import {
    createGig,
    fetchMyGig,
    updateGigDetails,
    // refreshInstagramDetails,
} from "../../../api/gigs";
import { Button } from "@mui/material";
import { Formik, Form } from "formik";
import * as Yup from "yup";

const EditGigDetails = () => {
    const [gigExist, setGigExist] = useState(false);
    const [gigDetails, setGigDetails] = useState({
        title: "",
        description: "",
        price: "",
        banner: "",
        followers: "",
        likes: "",
        comments: "",
    });

    useEffect(() => {
        const getGigDetails = async () => {
            try {
                const data = await fetchMyGig();
                setGigExist(true);
                setGigDetails(data.gig);
            } catch (error) {
                console.error("Failed to fetch gig details:", error);
            }
        };

        getGigDetails();
    }, []);

    const imageHandler = (e, setFieldValue) => {
        const fileReader = new FileReader();
        fileReader.onload = () => {
            if (fileReader.readyState === 2) {
                setFieldValue("banner", fileReader.result);
            }
        };
        fileReader.readAsDataURL(e.target.files[0]);
    };

    const validationSchema = Yup.object().shape({
        title: Yup.string().required("Title is required"),
        description: Yup.string().required("Description is required"),
        price: Yup.number()
            .required("Price is required")
            .positive("Price must be positive"),
        banner: Yup.string().required("Banner is required"),
        followers: Yup.number()
            .required("Followers is required")
            .positive("Followers must be positive"),
        likes: Yup.number()
            .required("Likes is required")
            .positive("Likes must be positive"),
        comments: Yup.number()
            .required("Comments is required")
            .positive("Comments must be positive"),
    });

    const handleSubmit = async (values, { setSubmitting }) => {
        if (gigExist) {
            try {
                const { userId, date, __v, ...filteredGigDetails } = values;
                await updateGigDetails(filteredGigDetails);
            } catch (error) {
                console.error("Failed to update gig details:", error);
            }
        } else {
            try {
                await createGig(values);
            } catch (error) {
                console.error("Failed to create gig details:", error);
            }
        }
        setSubmitting(false);
    };

    // const handleRefreshRequest = async () => {
    //     await refreshInstagramDetails();
    // };

    return (
        <div>
            <div className="w-full flex justify-center">
                <div className="relative">
                    <Formik
                        initialValues={gigDetails}
                        validationSchema={validationSchema}
                        onSubmit={handleSubmit}
                        enableReinitialize
                    >
                        {({
                            setFieldValue,
                            values,
                            handleChange,
                            handleBlur,
                            errors,
                            touched,
                            isSubmitting,
                        }) => (
                            <Form className="form-body">
                                <label htmlFor="banner">
                                    <img
                                        src={
                                            values.banner ||
                                            "images/upload-file-icon.jpg"
                                        }
                                        alt=""
                                        width={values?.banner ? 890 : 300}
                                        height={values?.banner ? 487 : 250}
                                        className="image-update border"
                                    />
                                    <div className="edit-icon">
                                        <MdEdit size={30} />
                                    </div>
                                </label>
                                <input
                                    type="file"
                                    id="banner"
                                    hidden
                                    onChange={(e) =>
                                        imageHandler(e, setFieldValue)
                                    }
                                    accept="image/png, image/jpg, image/jpeg, image/webp"
                                />
                                <div className="input-field-area-gig-page">
                                    <InputField
                                        label="Title"
                                        name="title"
                                        value={values.title}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        error={errors.title}
                                        touched={touched.title}
                                    />
                                    <TextArea
                                        label="Description"
                                        name="description"
                                        value={values.description}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        error={errors.description}
                                        touched={touched.description}
                                    />
                                    <InputField
                                        label="Price"
                                        name="price"
                                        type="number"
                                        value={values.price}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        error={errors.price}
                                        touched={touched.price}
                                    />
                                    <InputField
                                        label="No. of Followers"
                                        name="followers"
                                        type="number"
                                        value={values.followers}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        error={errors.followers}
                                        touched={touched.followers}
                                    />
                                    <InputField
                                        label="No. of Likes"
                                        name="likes"
                                        type="number"
                                        value={values.likes}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        error={errors.likes}
                                        touched={touched.likes}
                                    />
                                    <InputField
                                        label="No. of Comments"
                                        name="comments"
                                        type="number"
                                        value={values.comments}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        error={errors.comments}
                                        touched={touched.comments}
                                    />
                                    <Button
                                        variant="outlined"
                                        type="submit"
                                        className="submit-button"
                                        disableElevation
                                        disableRipple
                                        disabled={isSubmitting}
                                    >
                                        {gigExist ? "Update" : "Create"}
                                    </Button>
                                    {/* {gigExist && (
                                        <Button
                                            variant="outlined"
                                            className="submit-button"
                                            disableElevation
                                            disableRipple
                                            onClick={handleRefreshRequest}
                                        >
                                            Refresh Instagram Follower
                                        </Button>
                                    )} */}
                                </div>
                            </Form>
                        )}
                    </Formik>
                </div>
            </div>
        </div>
    );
};

export default EditGigDetails;
