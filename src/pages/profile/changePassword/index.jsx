import React from "react";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import InputField from "../../../components/common/inputField";
import "./style.css";
import { useDispatch } from "react-redux";
import { changePassword } from "../../../features/user/userSlice";

const ChangePassword = () => {
    const dispatch = useDispatch();

    const validationSchema = Yup.object().shape({
        oldPassword: Yup.string().required("Old password is required"),
        newPassword: Yup.string()
            .min(6, "Password must be at least 6 characters")
            .required("New password is required"),
        confirmPassword: Yup.string()
            .oneOf([Yup.ref("newPassword"), null], "Passwords must match")
            .required("Confirm password is required"),
    });

    const passwordChangeHandler = async (
        values,
        { setSubmitting, resetForm }
    ) => {
        try {
            await dispatch(
                changePassword({
                    currentPassword: values.oldPassword,
                    newPassword: values.newPassword,
                })
            ).unwrap();
            resetForm();
        } catch (error) {
            console.error("Failed to update password", error);
        } finally {
            setSubmitting(false);
        }
    };

    return (
        <div className="mt-10">
            <Formik
                initialValues={{
                    oldPassword: "",
                    newPassword: "",
                    confirmPassword: "",
                }}
                validationSchema={validationSchema}
                onSubmit={passwordChangeHandler}
            >
                {({
                    isSubmitting,
                    handleChange,
                    handleBlur,
                    values,
                    errors,
                    touched,
                }) => (
                    <Form>
                        <div className="input-field-area">
                            <InputField
                                label="Enter your old Password"
                                name="oldPassword"
                                type="password"
                                value={values.oldPassword}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                error={errors.oldPassword}
                                touched={touched.oldPassword}
                            />
                            <InputField
                                label="Enter your new Password"
                                name="newPassword"
                                type="password"
                                value={values.newPassword}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                error={errors.newPassword}
                                touched={touched.newPassword}
                            />
                            <InputField
                                label="Confirm your new password"
                                name="confirmPassword"
                                type="password"
                                value={values.confirmPassword}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                error={errors.confirmPassword}
                                touched={touched.confirmPassword}
                            />
                            <div>
                                <input
                                    type="submit"
                                    value="Update"
                                    disabled={isSubmitting}
                                    required
                                    className="submit-button"
                                />
                            </div>
                        </div>
                    </Form>
                )}
            </Formik>
        </div>
    );
};

export default ChangePassword;
