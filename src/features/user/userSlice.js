import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import {
    loginUser,
    updateUserProfile,
    updateUserPassword,
    uploadAvatar,
} from "../../api/user";

export const login = createAsyncThunk(
    "user/login",
    async (userData, { rejectWithValue }) => {
        try {
            const user = await loginUser(userData);
            localStorage.setItem("token", user.token);
            localStorage.setItem("user", JSON.stringify(user.user));
            return user.user;
        } catch (error) {
            return rejectWithValue(error.response.data);
        }
    }
);

export const updateUser = createAsyncThunk(
    "user/updateProfile",
    async (userData, { rejectWithValue }) => {
        try {
            const updatedUser = await updateUserProfile(userData);
            localStorage.setItem("user", JSON.stringify(updatedUser.user));
            return updatedUser.user;
        } catch (error) {
            return rejectWithValue(error.response.data);
        }
    }
);

export const changePassword = createAsyncThunk(
    "user/changePassword",
    async (passwordData, { rejectWithValue }) => {
        try {
            await updateUserPassword(passwordData);
        } catch (error) {
            return rejectWithValue(error.response.data);
        }
    }
);

export const uploadAvatarThunk = createAsyncThunk(
    "user/uploadAvatar",
    async (formData, { rejectWithValue }) => {
        try {
            const response = await uploadAvatar(formData);
            localStorage.setItem("user", JSON.stringify(response.user));
            return response.user;
        } catch (error) {
            return rejectWithValue(error.response.data);
        }
    }
);

const userSlice = createSlice({
    name: "user",
    initialState: {
        user: JSON.parse(localStorage.getItem("user")) || null,
        isLoading: false,
        error: null,
    },
    reducers: {
        logout: (state) => {
            state.user = null;
            localStorage.removeItem("token");
            localStorage.removeItem("user");
        },
        setUser: (state, action) => {
            state.user = action.payload;
        },
        rehydrate: (state) => {
            const user = JSON.parse(localStorage.getItem("user"));
            if (user) {
                state.user = user;
            }
        },
        setLoading: (state, action) => {
            state.isLoading = action.payload;
        },
    },
});

export const { logout, setUser, rehydrate, setLoading } = userSlice.actions;
export default userSlice.reducer;
