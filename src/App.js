import React from "react";
import { useSelector } from "react-redux";
import { Route, Routes } from "react-router-dom";
import Loading from "./components/common/loading";
import MainLayout from "./components/layouts/MainLayout";
import Landing from "./pages/landing";
import FAQ from "./pages/faq";
import Gigs from "./pages/gigs";
import GigDetails from "./pages/gigDetails";
import PrivateRoute from "./components/privateRoute";
import Profile from "./pages/profile";
import Orders from "./pages/orders";
import OrderDetails from "./pages/orderDetails";
import Chat from "./pages/chat";
import NotFound from "./pages/notFound";

const App = () => {
    const isLoading = useSelector((state) => state.user.isLoading);

    return (
        <div className="App">
            {isLoading && <Loading />}

            <Routes>
                <Route path="/" element={<MainLayout />}>
                    <Route index element={<Landing />} />
                    <Route path="/faq" element={<FAQ />} />
                    <Route path="gigs">
                        <Route index element={<Gigs />} />
                        <Route path=":id" element={<GigDetails />} />
                    </Route>
                    <Route
                        path="/profile"
                        element={
                            <PrivateRoute>
                                <Profile />
                            </PrivateRoute>
                        }
                    />
                    <Route path="order">
                        <Route
                            index
                            element={
                                <PrivateRoute>
                                    <Orders />
                                </PrivateRoute>
                            }
                        />
                        <Route path=":id" element={<OrderDetails />} />
                    </Route>
                    <Route
                        path="/chat"
                        element={
                            <PrivateRoute>
                                <Chat />
                            </PrivateRoute>
                        }
                    />
                    <Route path="*" element={<NotFound />} />
                </Route>
            </Routes>
        </div>
    );
};

export default App;
