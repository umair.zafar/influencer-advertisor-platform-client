import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { setUser, logout } from './features/user/userSlice';
import App from './App';

const Root = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    const user = localStorage.getItem('user');
    if (user) {
      dispatch(setUser(JSON.parse(user)));
    } else {
      dispatch(logout());
    }
  }, [dispatch]);

  return <App />;
};

export default Root;
