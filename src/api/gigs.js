import axios from "axios";
import toast from "react-hot-toast";
import { useNavigate } from "react-router-dom";

const API = axios.create({
    baseURL: "influencer-advertisor-platform-server.vercel.app/app/v1/",
});

API.interceptors.request.use(
    (config) => {
        const token = localStorage.getItem("token");
        if (token) {
            config.headers["Authorization"] = `Bearer ${token}`;
        }
        return config;
    },
    (error) => {
        return Promise.reject(error);
    }
);

API.interceptors.response.use(
    (response) => {
        if (response.data.message) {
            toast.success(response.data.message);
        }

        return response;
    },
    (error) => {
        toast.error(error.response?.data?.message || error.message);

        if (error.response.status === 401) {
            localStorage.removeItem("token");
            localStorage.removeItem("user");
            const navigate = useNavigate();
            navigate("/");
        }
        return Promise.reject(error);
    }
);
export const fetchAllGigs = async ({ page = 1, limit = 10, search = '', minBudget = '', maxBudget = '' }) => {
    const response = await API.get(`/gig?page=${page}&limit=${limit}&search=${search}&minBudget=${minBudget}&maxBudget=${maxBudget}`);
    return response.data;
};

export const fetchGigDetails = async (id) => {
    const response = await API.get(`gig/${id}`);
    return response.data;
};

export const fetchMyGig = async () => {
    const response = await API.get("/gig/me");
    return response.data;
};

export const updateGigDetails = async (updatedData) => {
    const response = await API.put(`/gig/update/`, updatedData);
    return response.data;
};

export const createGig = async (data) => {
    const response = await API.post(`/gig/insert`, data);
    return response.data;
}

export const refreshInstagramDetails = async () => {
    const response = await API.post(`/gig/refresh/me`);
    return response.data;
}