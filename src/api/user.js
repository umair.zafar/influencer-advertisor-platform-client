import axios from "axios";
import { logout } from "../features/user/userSlice";
import toast from "react-hot-toast";
import { useNavigate } from "react-router-dom";

const API = axios.create({
    baseURL: "influencer-advertisor-platform-server.vercel.app/app/v1/",
});

API.interceptors.request.use(
    (config) => {
        const token = localStorage.getItem("token");
        if (token) {
            config.headers["Authorization"] = `Bearer ${token}`;
        }
        return config;
    },
    (error) => {
        return Promise.reject(error);
    }
);

API.interceptors.response.use(
    (response) => {
        if (response.data.message) {
            toast.success(response.data.message);
        }
        return response;
    },
    (error) => {
        toast.error(error.response?.data?.message || error.message);

        if (error.response.status === 401) {
            localStorage.removeItem("token");
            localStorage.removeItem("user");
            window.location.reload();
        }
        return Promise.reject(error.response.status);
    }
);

export const registerUser = async (userData) => {
    const response = await API.post("user", userData);
    return response.data.user;
};

export const loginUser = async (userData) => {
    const response = await API.post("user/login", userData);
    return response.data;
};
export const updateUserProfile = async (userData) => {
    const response = await API.put("user/update", userData);
    return response.data;
};

export const uploadAvatar = async (formData) => {
    const response = await API.patch("user/update-avatar", formData);
    return response.data;
};

export const updateUserPassword = async (passwordData) => {
    const response = await API.patch("user/change-password", passwordData);
    return response.data;
};

export const logoutUser = async (dispatch) => {
    try {
        const response = await API.post("user/logout");
        return response.data;
    } catch (error) {
        dispatch(logout());
        return Promise.reject(error);
    }
};

export const addChat = async (data) => {
    const response = await API.post("user/chat/add", { chatterId: data });
    return response.data;
};

export const getAllChats = async (searchContact) => {
    const response = await API.get(`user/chat/all?search=${searchContact}`);
    return response.data;
};
