import database from "../utils/firebase";
import {
    ref,
    push,
    onChildAdded,
    onValue,
    serverTimestamp,
} from "firebase/database";

export const sendMessage = (chatId, fromUserId, toUserId, text) => {
    const message = {
        senderId: fromUserId,
        receiverId: toUserId,
        text: text,
        timestamp: serverTimestamp(),
    };
    const messagesRef = ref(database, `chats/${chatId}/messages`);
    return push(messagesRef, message);
};

export const listenForMessages = (chatId, callback) => {
    const messagesRef = ref(
        database,
        `chats/${chatId}/messages`
    );
    const unsubscribe = onValue(messagesRef, (snapshot) => {
        const messages = [];
        snapshot.forEach((messageSnapshot) => {
            messages.push(messageSnapshot.val());
        });
        callback(messages);
    });
    return unsubscribe;
};
