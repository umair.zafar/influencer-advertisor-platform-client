// src/api/order.js

import axios from "axios";
import toast from "react-hot-toast";

const API = axios.create({
    baseURL: "influencer-advertisor-platform-server.vercel.app",
});

API.interceptors.request.use(
    (config) => {
        const token = localStorage.getItem("token");
        if (token) {
            config.headers["Authorization"] = `Bearer ${token}`;
        }
        return config;
    },
    (error) => {
        return Promise.reject(error);
    }
);

API.interceptors.response.use(
    (response) => {
        if (response.data.message) {
            toast.success(response.data.message);
        }

        return response;
    },
    (error) => {
        toast.error(error.response?.data?.message || error.message);

        if (error.response.status === 401) {
            localStorage.removeItem("token");
            localStorage.removeItem("user");
            window.location.reload();
        }
        return Promise.reject(error);
    }
);

export const fetchOrders = async () => {
    try {
        const response = await API.get("/app/v1/order");
        return response.data;
    } catch (error) {
        console.error("Error fetching orders", error);
        throw error;
    }
};

export const submitReview = async (id, data) => {
    try {
        const response = await API.post(`/app/v1/order/review/${id}`, data);
        return response.data;
    } catch (error) {
        console.error("Error adding review", error);
        throw error;
    }
};

export const updateOrderStatus = async (id, data) => {
    try {
        const response = await API.patch(`/app/v1/order/status/${id}`, data);
        return response.data;
    } catch (error) {
        console.error("Error updating order status", error);
        throw error;
    }
};

export const createPaymentSession = async (orderId) => {
    try {
        const response = await API.post(`/app/v1/order/payment-session`, {orderId});
        return response;
    } catch (error) {
        console.error("Error creating payment session", error);
        throw error;
    }
};

export const createOrder = async (data) => {
    try {
        const response = await API.post("/app/v1/order", data);
        return response.data;
    } catch (error) {
        console.error("Error creating order", error);
        throw error;
    }
};
