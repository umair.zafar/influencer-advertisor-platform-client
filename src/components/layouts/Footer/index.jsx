import React from "react";
import "./style.css";
import { FaFacebook, FaInstagram, FaLinkedin } from "react-icons/fa";
import { RiTwitterXFill } from "react-icons/ri";

const Footer = () => {
    return (
        <footer className="footer footer-color footer-body mt-5 p-5">
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-2">
                        <h5 className="poppins-bold"> Info</h5>
                        <ul className="list-unstyled poppins-regular">
                            <li>About Us</li>
                            <li>Contact Us</li>
                            <li>FAQ</li>
                        </ul>
                    </div>
                    <div className="col-md-2">
                        <h5 className="poppins-bold">Features</h5>
                        <ul className="list-unstyled poppins-regular">
                            <li>Feature 1</li>
                            <li>Feature 2</li>
                            <li>Feature 3</li>
                        </ul>
                    </div>
                    <div className="col-md-4">
                        <h5 className="poppins-bold">About Our Platform</h5>
                        <p className="poppins-regular">
                            Our platform connects advertisers with influencers
                            to create impactful marketing campaigns. We provide
                            comprehensive tools to manage, track, and optimize
                            your marketing efforts, ensuring maximum reach and
                            engagement.
                        </p>
                        <p className="poppins-regular">
                            Whether you're a brand looking to boost your
                            presence or an influencer seeking new opportunities,
                            our platform offers a seamless experience tailored
                            to your needs.
                        </p>
                    </div>
                    <div className="col-md-4">
                        <h5 className="poppins-bold">Why Choose Us</h5>
                        <p className="poppins-regular">
                            With our advanced analytics and extensive network of
                            influencers, we help you identify the perfect
                            matches for your campaigns. Our user-friendly
                            interface and dedicated support team ensure that
                            your marketing efforts are effective and
                            hassle-free.
                        </p>
                        <p className="poppins-regular">
                            Join us today and discover the power of influencer
                            marketing to drive your brand's success.
                        </p>
                    </div>
                </div>
                <hr />
                <div className="row">
                    <div className="col-md-3">
                        <p>
                            <span className="poppins-bold">InfluenceX</span> ©{" "}
                            {new Date().getFullYear()}{" "}
                        </p>
                    </div>
                    <div className="col-md-9 text-end">
                        <a
                            href="https://twitter.com"
                            className="me-3 social-color"
                        >
                            <RiTwitterXFill />
                        </a>
                        <a
                            href="https://facebook.com"
                            className="me-3 social-color"
                        >
                            <FaFacebook />
                        </a>
                        <a
                            href="https://linkedin.com"
                            className="me-3 social-color"
                        >
                            <FaLinkedin />
                        </a>
                        <a
                            href="https://instagram.com"
                            className="me-3 social-color"
                        >
                            <FaInstagram />
                        </a>
                    </div>
                </div>
            </div>
        </footer>
    );
};

export default Footer;
