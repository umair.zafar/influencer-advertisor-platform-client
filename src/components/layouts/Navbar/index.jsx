import React, { useState } from "react";
import { useSelector } from "react-redux";
import "./style.css";
import { IoMdPeople } from "react-icons/io";
import { Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import AppLogo from "../../../assets/icons/logo.png";

const Navbar = ({ handleShowLogin, handleShowRegister, activeTab }) => {
    const user = useSelector((state) => state.user.user);
    const [isToggled, setIsToggled] = useState(false);

    const toggleMenu = () => {
        setIsToggled(!isToggled);
    };

    return (
        <nav
            className="navbar navbar-expand-lg navbar-light header-color fixed-top common-shadow"
            bg="dark"
            data-bs-theme="dark"
        >
            <div className="container-fluid">
                <a className="navbar-brand px-3" href="/">
                    <img src={AppLogo} alt="logo" width={70} />
                </a>
                <button
                    className={`navbar-toggler ${isToggled ? "open" : ""}`}
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent"
                    aria-expanded={isToggled}
                    aria-label="Toggle navigation"
                    onClick={toggleMenu}
                >
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div
                    className="collapse navbar-collapse justify-content-end"
                    id="navbarSupportedContent"
                >
                    <ul className="navbar-nav mb-2 mb-lg-0 gap-3">
                        <li className="nav-item">
                            <Link
                                className={`nav-link navbar-text ${
                                    activeTab === "" ? "active-tab" : undefined
                                }`}
                                to="/"
                            >
                                Home
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link
                                className={`nav-link navbar-text ${
                                    activeTab === "gigs"
                                        ? "active-tab"
                                        : undefined
                                }`}
                                to="/gigs"
                            >
                                Gigs
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link
                                className={`nav-link navbar-text ${
                                    activeTab === "faq"
                                        ? "active-tab"
                                        : undefined
                                }`}
                                to="/faq"
                            >
                                FAQ
                            </Link>
                        </li>
                        {user ? (
                            <>
                                <li className="nav-item">
                                    <Link
                                        className={`nav-link navbar-text ${
                                            activeTab === "chat"
                                                ? "active-tab"
                                                : undefined
                                        }`}
                                        to="/chat"
                                    >
                                        Chats
                                    </Link>
                                </li>
                                <li className="nav-item">
                                    <Link
                                        className={`nav-link navbar-text ${
                                            activeTab === "order"
                                                ? "active-tab"
                                                : undefined
                                        }`}
                                        to="/order"
                                    >
                                        Orders
                                    </Link>
                                </li>
                                <Link className={`profile-btn`} to="/profile">
                                    Profile
                                    <IoMdPeople size={25} />
                                </Link>
                            </>
                        ) : (
                            <>
                                <Link
                                    className="nav-link navbar-text"
                                    onClick={handleShowLogin}
                                >
                                    Login
                                </Link>
                                <Button
                                    variant="outline-light"
                                    className="search-border"
                                    onClick={handleShowRegister}
                                >
                                    Join
                                    <IoMdPeople size={25} />
                                </Button>
                            </>
                        )}
                    </ul>
                </div>
            </div>
        </nav>
    );
};

export default Navbar;
