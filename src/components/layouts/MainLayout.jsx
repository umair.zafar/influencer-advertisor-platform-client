import React, { useState } from "react";
import { Outlet, useLocation } from "react-router-dom";
import Navbar from "./Navbar";
import Login from "../auth/login";
import Footer from "./Footer";
import "./style.css";
import { Toaster } from "react-hot-toast";
import RegisterModal from "../auth/register";

const MainLayout = () => {
    const [showLoginModal, setShowLoginModal] = useState(false);
    const [showRegisterModal, setShowRegisterModal] = useState(false);

    const location = useLocation();

    const path = location.pathname;
    const parts = path.split("/");
    const activeTab = parts[1];

    const handleShowLogin = () => setShowLoginModal(true);
    const handleCloseLogin = () => setShowLoginModal(false);

    const handleShowRegister = () => setShowRegisterModal(true);
    const handleCloseRegister = () => setShowRegisterModal(false);

    return (
        <div className="main-layout">
            {/* Top Nav */}
            <div className="top-nav">
                <Navbar
                    handleShowLogin={handleShowLogin}
                    handleShowRegister={handleShowRegister}
                    activeTab={activeTab}
                />
            </div>
            {/* Content Area */}
            <div className="content mt-15">
                <Outlet />
            </div>

            {/* Footer */}
            <div className="footer">
                <Footer />
            </div>
            <Login
                show={showLoginModal}
                handleClose={handleCloseLogin}
                handleRegisterShow={handleShowRegister}
            />
            <RegisterModal
                show={showRegisterModal}
                handleClose={handleCloseRegister}
                handleLoginShow={handleShowLogin}
            />

            <Toaster position="top-center" reverseOrder={false} />
        </div>
    );
};

export default MainLayout;
