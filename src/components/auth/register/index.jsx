import React from "react";
import { FormSelect, Modal } from "react-bootstrap";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { registerUser } from "../../../api/user";
import { setUser } from "../../../features/user/userSlice";
import { Formik, Form as FormikForm } from "formik";
import * as Yup from "yup";
import InputField from "../../common/inputField";
import { Button } from "@mui/material";

const RegisterModal = ({ show, handleClose, handleLoginShow }) => {
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const handleSubmit = async (values, { setSubmitting }) => {
        const formData = new FormData();
        for (const key in values) {
            formData.append(key, values[key]);
        }
        if (values.avatar) {
            formData.append("avatar", values.avatar);
        }

        try {
            const user = await registerUser(formData);
            dispatch(setUser(user));
            handleClose();
            navigate("/");
        } catch (error) {
            console.error("Error registering user:", error);
        } finally {
            setSubmitting(false);
        }
    };

    const validationSchema = Yup.object().shape({
        name: Yup.string().required("Name is required"),
        email: Yup.string()
            .email("Invalid email")
            .required("Email is required"),
        password: Yup.string().required("Password is required"),
        country: Yup.string().required("Country is required"),
        city: Yup.string().required("City is required"),
        role: Yup.string()
            .oneOf(["advertiser", "influencer"])
            .required("Role is required"),
        facebookId: Yup.string().optional(),
    });

    return (
        <Modal show={show} onHide={handleClose} centered>
            <Modal.Header className="flex justify-center">
                <Modal.Title>Register</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Formik
                    initialValues={{
                        name: "",
                        email: "",
                        password: "",
                        country: "",
                        city: "",
                        avatar: null,
                        role: "advertiser",
                        facebookId: "",
                    }}
                    validationSchema={validationSchema}
                    onSubmit={handleSubmit}
                >
                    {({
                        values,
                        handleChange,
                        handleBlur,
                        setFieldValue,
                        errors,
                        touched,
                        isSubmitting,
                    }) => (
                        <FormikForm>
                            <InputField
                                label="Name"
                                name="name"
                                type="text"
                                value={values.name}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                error={errors.name}
                                touched={touched.name}
                            />
                            <InputField
                                label="Email"
                                name="email"
                                type="email"
                                value={values.email}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                error={errors.email}
                                touched={touched.email}
                            />
                            <InputField
                                label="Password"
                                name="password"
                                type="password"
                                value={values.password}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                error={errors.password}
                                touched={touched.password}
                            />
                            <InputField
                                label="Country"
                                name="country"
                                type="text"
                                value={values.country}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                error={errors.country}
                                touched={touched.country}
                            />
                            <InputField
                                label="City"
                                name="city"
                                type="text"
                                value={values.city}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                error={errors.city}
                                touched={touched.city}
                            />
                            <label htmlFor="">Role</label>
                            <FormSelect
                                label="Role"
                                name="role"
                                value={values.role}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                error={errors.role}
                                touched={touched.role}
                            >
                                <option value="advertiser">Advertiser</option>
                                <option value="influencer">Influencer</option>
                            </FormSelect>
                            <br />
                            <InputField
                                label="Facebook ID"
                                name="facebookId"
                                type="text"
                                value={values.facebookId}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                error={errors.facebookId}
                                touched={touched.facebookId}
                            />
                            <Button
                                variant="contained"
                                type="submit"
                                className="mt-2"
                                disableRipple
                                fullWidth
                                disableElevation
                                disabled={isSubmitting}
                            >
                                <b> Register </b>
                            </Button>
                        </FormikForm>
                    )}
                </Formik>
                <Button
                    variant="outlined"
                    className="mt-2"
                    disableRipple
                    disableElevation
                    fullWidth
                    onClick={() => {
                        handleClose();
                        handleLoginShow();
                    }}
                >
                    Already have an account? Login
                </Button>
            </Modal.Body>
        </Modal>
    );
};

export default RegisterModal;
