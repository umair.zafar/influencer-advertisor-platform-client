import React, { useEffect } from "react";
import { Modal } from "react-bootstrap";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { FaInstagram } from "react-icons/fa";
import { login } from "../../../features/user/userSlice";
import InputField from "../../common/inputField";
import { Formik, Form as FormikForm } from "formik";
import * as Yup from "yup";
import "./style.css";
import { Button } from "@mui/material";

const CLIENT_ID = "352075587463723";
const REDIRECT_URI = "https://localhost:3000";
const INSTAGRAM_AUTH_URL = `https://api.instagram.com/oauth/authorize?client_id=${CLIENT_ID}&redirect_uri=${REDIRECT_URI}&scope=user_profile,user_media&response_type=code`;

const Login = ({ show, handleClose, handleRegisterShow }) => {
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const handleInstagramLogin = () => {
        // window.location.href = INSTAGRAM_AUTH_URL;
    };

    useEffect(() => {
        // const urlParams = new URLSearchParams(window.location.search);
        // const code = urlParams.get("code");

        // if (code) {
        //     fetchAccessToken(code);
        // }
    }, []);

    const fetchAccessToken = async (code) => {
        // try {
        //     const response = await fetch('/app/v1/auth/instagram/token', {
        //         method: 'POST',
        //         headers: { 'Content-Type': 'application/json' },
        //         body: JSON.stringify({ code, redirect_uri: REDIRECT_URI }),
        //     });
        //     const data = await response.json();
        //     if (data.access_token) {
        //         fetchInstagramProfile(data.access_token);
        //     } else {
        //         console.error('Failed to get access token');
        //     }
        // } catch (error) {
        //     console.error('Failed to fetch access token:', error);
        // }
    };

    const fetchInstagramProfile = async (accessToken) => {
        // try {
        //     const response = await fetch(`/app/v1/auth/instagram/profile?access_token=${accessToken}`);
        //     const data = await response.json();
        //     if (data.token) {
        //         dispatch(login(data.user));
        //         handleClose();
        //         navigate('/');
        //     } else {
        //         console.error('Failed to fetch Instagram profile');
        //     }
        // } catch (error) {
        //     console.error('Failed to fetch Instagram profile:', error);
        // }
    };

    const handleSubmit = async (values, { setSubmitting }) => {
        try {
            await dispatch(login(values)).unwrap();
            handleClose();
            window.location.reload();
        } catch (error) {
            console.error("Failed to login:", error);
        } finally {
            setSubmitting(false);
        }
    };

    const validationSchema = Yup.object().shape({
        email: Yup.string()
            .email("Invalid email")
            .required("Email is required"),
        password: Yup.string().required("Password is required"),
    });

    return (
        <div>
            <Modal show={show} onHide={handleClose} centered>
                <Modal.Header className="flex justify-center">
                    <Modal.Title>Login</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Formik
                        initialValues={{ email: "", password: "" }}
                        validationSchema={validationSchema}
                        onSubmit={handleSubmit}
                    >
                        {({
                            values,
                            handleChange,
                            handleBlur,
                            errors,
                            touched,
                            isSubmitting,
                        }) => (
                            <FormikForm className="form-body">
                                <InputField
                                    label="Email"
                                    name="email"
                                    type="email"
                                    value={values.email}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    error={errors.email}
                                    touched={touched.email}
                                />
                                <InputField
                                    label="Password"
                                    name="password"
                                    type="password"
                                    value={values.password}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    error={errors.password}
                                    touched={touched.password}
                                />
                                <Button
                                    type="submit"
                                    variant="contained"
                                    disableRipple
                                    disableElevation
                                    block
                                    disabled={isSubmitting}
                                >
                                    <b>Login</b>
                                </Button>
                                <Button
                                    variant="outlined"
                                    disableRipple
                                    disableElevation
                                    onClick={() => {
                                        handleClose();
                                        handleRegisterShow();
                                    }}
                                >
                                    Register
                                </Button>
                                <Button
                                    variant="outlined"
                                    disableRipple
                                    disableElevation
                                    className="insta-btn"
                                    onClick={handleInstagramLogin}
                                    block
                                >
                                    <FaInstagram size={35} />
                                </Button>
                            </FormikForm>
                        )}
                    </Formik>
                </Modal.Body>
            </Modal>
        </div>
    );
};

export default Login;
