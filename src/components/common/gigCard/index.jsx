import React from "react";
import { useNavigate } from "react-router-dom";
import { AiFillStar } from "react-icons/ai";
import "./style.css";

const GigCard = ({ gig }) => {
    const navigate = useNavigate();

    const handleClick = () => {
        navigate(`/gigs/${gig._id}`);
    };

    return (
        <div className="gig-card" onClick={handleClick}>
            <img src={gig.banner} alt="Gig" className="gig-image" />
            <div className="gig-content">
                <div className="gig-header">
                    <div className="gig-seller">{gig.userId.name}</div>
                    <div className="gig-rating">
                        <AiFillStar /> {gig.averageRating}
                    </div>
                </div>
                <div className="gig-title">{gig.title}</div>
                <div className="gig-stats">
                    <div className="gig-followers">
                        {gig.followers} Followers
                    </div>
                    <div className="gig-posts">{gig.likes} Total Likes</div>
                    <div className="gig-likes">
                        {gig.comments} Total Comments
                    </div>
                </div>
                <div className="gig-footer">
                    <div className="gig-price">Price ${gig.price}</div>
                </div>
            </div>
        </div>
    );
};

export default GigCard;
