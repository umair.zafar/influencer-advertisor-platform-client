import React, { useState } from "react";
import { FaEye, FaEyeSlash } from "react-icons/fa";
import "./style.css";

const InputField = ({
    label,
    required = false,
    value,
    readonly = false,
    placeholder = "",
    name,
    onChange,
    onBlur,
    type = "text",
    error,
    touched,
}) => {
    const [showPassword, setShowPassword] = useState(false);

    const togglePasswordVisibility = () => {
        setShowPassword((prevShowPassword) => !prevShowPassword);
    };

    return (
        <div className="field-container w-full">
            <label className="input-label">{label}</label>
            <div className="input-group">
                <input
                    type={type === "password" && showPassword ? "text" : type}
                    name={name}
                    required={required}
                    value={value}
                    readOnly={readonly}
                    placeholder={placeholder}
                    onChange={onChange}
                    onBlur={onBlur}
                    className={`form-control ${error && touched ? "is-invalid" : ""}`}
                    />
                    {type === "password" && (
                        <button
                            type="button"
                            className={`btn ${(error && touched )? 'btn-outline-danger': "btn-outline-secondary"}`}
                            onClick={togglePasswordVisibility}
                        >
                            {showPassword ? <FaEyeSlash /> : <FaEye />}
                        </button>
                    )}
                {error && touched && (
                    <div className="invalid-feedback">{error}</div>
                )}
            </div>
        </div>
    );
};

export default InputField;
