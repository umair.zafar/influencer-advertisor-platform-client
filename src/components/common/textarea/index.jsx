import React from "react";
import "./style.css";

const TextArea = ({
  label,
  required = false,
  value,
  readOnly = false,
  name,
  onChange,
  onBlur,
  error,
  touched,
}) => {
  return (
    <div className="field-container">
      <label className="input-label">{label}</label>
      <textarea
        name={name}
        required={required}
        value={value}
        readOnly={readOnly}
        onChange={onChange}
        onBlur={onBlur}
        className={`form-control ${error && touched ? "is-invalid" : ""}`}
      />
      {error && touched && (
        <div className="invalid-feedback">{error}</div>
      )}
    </div>
  );
};

export default TextArea;
