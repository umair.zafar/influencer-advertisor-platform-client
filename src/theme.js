import { createTheme } from '@mui/material/styles';

const theme = createTheme({
  palette: {
    primary: {
      main: '#3c4f8d', 
    },
    grey: {
      main: '#555'
    }
  },
});

export default theme;